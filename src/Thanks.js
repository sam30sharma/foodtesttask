import React from 'react';


const Thanks = () => {
    return (
        <div className="row thank">
            <h1 className="col-sm-12">
                Thank You!<br /><span className="emoj">&#128522;</span>
                <br /><br />
               Enjoy Your Meal
           </h1>

        </div>

    )
}
export default Thanks;