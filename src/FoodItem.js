import React, { useState } from 'react';
import image1 from './image1.png';
import pick1 from './pick1.jpg';
import pick2 from './pick2.jpg';
import pick3 from './pick3.jpg';
import pick4 from './pick4.jpg';

const FoodItem = () => {

    const [count, setCount] = useState(100)
    const [count1, setCount1] = useState(1)
    const [count01, setCount01] = useState(180)
    const [count11, setCount11] = useState(1)

    const [counta, setCounta] = useState(160)
    const [countab, setCountab] = useState(1)

    const [countc, setCountc] = useState(210)
    const [countcd, setCountcd] = useState(1)

    let pick01 = () => {
        setCount(count + 100)
        setCount1(count1 + 1)
    }
    let pick011 = () => {
        setCount(count - 100)
        setCount1(count1 - 1)
    }

    let pick11 = () => {
        setCount01(count01 + 180)
        setCount11(count11 + 1)
    }
    let pick12 = () => {
        setCount01(count01 - 180)
        setCount11(count11 - 1)
    }

    let pick03 = () => {
        setCounta(counta + 160)
        setCountab(countab + 1)
    }
    let pick003 = () => {
        setCounta(counta - 160)
        setCountab(countab - 1)
    }
    let pick04 = () => {
        setCountc(countc + 210)
        setCountcd(countcd + 1)
    }
    let pick004 = () => {
        setCountc(countc - 210)
        setCountcd(countcd - 1)
    }

    return (
        <div>
            <header>
                <div className="row">
                    <div className="col-sm-12 bgcolor">
                        <img src={image1} alt="Error" />
                        <h2> Food's Restaurant </h2>
                    </div>

                </div>
            </header>
            <div className="row row1">
                <div className="col-sm-3 col-md-6 col-lg-3 pick1">
                    <img src={pick1} alt="Error" />
                    <h4> burger</h4>
                    <h6>price:{count}</h6>
                    <h6>Quantity={count1}</h6>
                    <button onClick={(e) => pick01(e)} > +</button>
                    <button onClick={(e) => pick011(e)}>-</button>


                </div>
                <div className="col-sm-3 col-md-6 col-lg-3 pick2">
                    <img src={pick2} alt="Error" />
                    <h4> Chees Tost</h4>
                    <h6>price:{count01}</h6>
                    <h6>Quantity={count11}</h6>
                    <button onClick={(e) => pick11(e)} > +</button>
                    <button onClick={(e) => pick12(e)}>-</button>
                </div>
                <div className="col-sm-3 col-md-6 col-lg-3 pick3">
                    <img src={pick3} alt="Error" />
                    <h4> Fried Chips</h4>
                    <h6>price:{counta}</h6>
                    <h6>Quantity={countab}</h6>
                    <button onClick={(e) => pick03(e)} > +</button>
                    <button onClick={(e) => pick003(e)}>-</button>
                </div>
                <div className="col-sm-3 col-md-6 col-lg-3 pick4">
                    <img src={pick4} alt="Error" />
                    <h4> Fried Tost</h4>
                    <h6>price:{countc}</h6>
                    <h6>Quantity={countcd}</h6>
                    <button onClick={(e) => pick04(e)} > +</button>
                    <button onClick={(e) => pick004(e)}>-</button>
                </div>
            </div>
            <div className="row">
                <div className="col-3 col-sm-5 ">

                </div>
                <button className="col-6 col-sm-6 col-md-3 col-sm-3 col-lg-2 col-xl-2 MenuButtons">
                    <a href="/login/fooditem/thanks" > Order Now</a>

                </button>
                <div className="col-3 col-sm-5">

                </div>
            </div>
        </div>
    )
}
export default FoodItem;
