import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
// import FoodApp from './FoodApp';
// import FoodItem from './FoodItem';
// import Login from './Login';
import App from './App';
import { BrowserRouter } from "react-router-dom";

ReactDOM.render(
  <>
    <BrowserRouter>
      <App />
    </BrowserRouter>

    {/* <FoodApp /> */}
    {/* <FoodItem /> */}
    {/* <Login /> */}
  </>,
  document.getElementById('root')
);
