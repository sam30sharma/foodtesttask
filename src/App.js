import React from 'react';
import { Route, Switch } from 'react-router-dom';

import FoodApp from './FoodApp';
import Login from './Login';
import FoodItem from './FoodItem';
import Thanks from './Thanks';


const App = () => {
    return (
        <div>
            <Switch>
                <Route exact path="/" component={FoodApp} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/login/fooditem" component={FoodItem} />
                <Route exact path="/login/fooditem/thanks" component={Thanks} />
            </Switch>

        </div>

    )
}
export default App;